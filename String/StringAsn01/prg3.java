/* Write a code to check if entered string is palindrome */

class PalindromeDemo{
	static boolean myStrPalin(String str1){
		char arr1[] = str1.toCharArray();
		int end = arr1.length-1;
		for(int i=0; i<arr1.length/2; ){

			if(arr1[i]-arr1[end]==0 || arr1[i]-arr1[end]== 32 || arr1[i]- arr1[end]==-32){
				i++;
				end--;
			}else{
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args){

		String str1 = "MadAm";

		boolean ret = myStrPalin(str1);
		System.out.println(ret);
	}
}


