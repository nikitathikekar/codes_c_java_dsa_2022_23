/* Write a code to check if given two strings  are equal */

class StringEqualsDemo{
	
	static boolean myStringEquals(String str1, String str2){
		
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		
		if(arr1.length != arr2.length){
			return false;
		}else{
			for(int i=0; i<arr1.length; i++){
				if(arr1[i] != arr2[i]){
					return false;
				}
			}
			return true;
		}

	}
	public static void main(String[] args){

		String str1 ="nikita";
		String str2 ="nikitaThikekar";

		boolean ret = myStringEquals(str1,str2);
		System.out.println(ret);
	}
}
