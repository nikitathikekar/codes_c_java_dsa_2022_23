/* WAP to find count vowels in the entered String */

class CountVowels{
	static int myCountVowels(String str1){
		char arr1[] = str1.toCharArray();
		int count =0;
		for(int i=0; i<arr1.length; i++){
			if(arr1[i] == 'a' ||arr1[i] =='e' || arr1[i]=='i' ||arr1[i]=='o' ||arr1[i]=='u'||arr1[i]=='A'||arr1[i]=='E' ||arr1[i]=='I' || arr1[i]=='O'||arr1[i]=='U'){
				count++;
			}
		}
		return count;

	}
	public static void main(String[] args){
		String str1 = "NikitA";
	
		int count = myCountVowels(str1);
		System.out.println(count);
	}
}
