/*Write a code to reverse String */

class StringReverseDemo{
	
	char[] myStringReverse(String str2){

		char[] str3 =str2.toCharArray();
		int end = str3.length-1;
		for(int i=0; i<str3.length/2; i++){

			char temp = str3[i];
			str3[i] = str3[end];
			str3[end] = temp;

			end--;
		}
		return str3;
	}
	public static void main(String[] args){
		StringReverseDemo obj = new StringReverseDemo();
		String str1 = "Nikita";
		
		System.out.println(str1);
		
		char[] str2 = obj.myStringReverse(str1);
		
		System.out.println(str2);
	}
}

	


