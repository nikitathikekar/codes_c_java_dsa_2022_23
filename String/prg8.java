/*equalsIgnoreCase */

class EqualsIgnoreCaseDemo{
	static boolean myEqualsIC(String str1, String str2){
		
		char[] arr1 = str1.toCharArray();
		char[] arr2 = str2.toCharArray();

		if(arr1.length!= arr2.length){
			return false;
		}else{
			for(int i=0; i<arr1.length; ){
				if((arr1[i]-arr2[i] == 0) || (arr1[i]-arr2[i]==32) || (arr1[i]-arr2[i]==-32)){
					i++;
				}else{
					return false;
				}
			}
			return true;
		}
	}
	public static void main(String[] args){

		String str1 = "NikiTa";
		String str2 = "NIKItA";

		System.out.println(str1.equalsIgnoreCase(str2));

		boolean ret = myEqualsIC(str1,str2);
		System.out.println(ret);

	}
}
