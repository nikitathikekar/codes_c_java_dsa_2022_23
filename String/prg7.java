/* toCharArray */

class ToCharArrayDemo{

	public static void main(String[] args){

		String str1 = "Shashi Sir";
		char arr1[] = str1.toCharArray();

		for(int i=0; i<arr1.length; i++){
			System.out.print(arr1[i]+"\t");
		}
		System.out.println();
	}
}
