/* Method 2- length*/

class LengthDemo{
	
	static int myStrLen(String str1){
		char str2[] = str1.toCharArray();
		int count =0;
		for(int i=0; i<str2.length; i++){
			count++;
		}
		return count;
	}
	public static void main(String[] args){

		String str1 = "NikitaThikekar";
	//	System.out.println(str1.length());
	
		int ret = myStrLen(str1);

		System.out.println(ret);
	}
}
