/*Method 1- Concat */

class ConcatDemo{

	public static void main(String[] args){

		String str1 = "Nikita";
		String str2 = "Thikekar";

		/*String str3 = str1.concat(str2);

		System.out.println(str3);
		*/

		char[] str3 = str1.toCharArray();
		char[] str4 = str2.toCharArray();
		
		int len = str3.length + str4.length;
		char str5[]= new char[len];

		for(int i=0; i<str3.length; i++){
			str5[i] = str3[i];
		}
		for(int i=0;i<str4.length; i++){
			str5[str3.length+i] = str4[i];
		}

		System.out.println(str5);

	}
}

