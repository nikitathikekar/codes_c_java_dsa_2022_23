/* equals */

class EqualsDemo{
	boolean myEqualsDemo(String str1,String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		if(arr1.length != arr2.length){
			return false;
		}else{

			for(int i=0; i<arr1.length; i++){
				if(arr1[i] != arr2[i]){
					return false;
				}
			}
			return true;
		}
	}

	public static void main(String[] args){
		EqualsDemo obj =new EqualsDemo();

		String str1 = "Core2Web";
		String str2 =new String ("Core2Web");

		System.out.println(str1.equals(str2));

		boolean ret =obj.myEqualsDemo(str1,str2);
		System.out.println(ret);
	}
}

