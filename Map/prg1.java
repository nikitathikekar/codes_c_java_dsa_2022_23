/* Hashmap  */

import java.util.*;

class HashMapDemo{

	public static void main(String[] args){

		HashSet hs = new HashSet();
		hs.add("Rahul");
		hs.add("Badhe");
		hs.add("Ashish");
		hs.add("Kanha");

		System.out.println(hs);

		HashMap hm = new HashMap();
		hm.put("Kanha","Infosys");
		hm.put("Rahul","BMC");
		hm.put("Badhe","CarPro");
		hm.put("Ashish","Barclays");

		System.out.println(hm);


	}
}
