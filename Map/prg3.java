

import java.util.*;

class HashMapDemo2{

	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("RAM","Niki");
		hm.put("KAM","Sam");
		hm.put("SAM","Shreya");

		System.out.println(hm);
		

		String str1="RAM";
		String str2 = "KAM";
		String str3 = "SAM";

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		
		Integer a=new Integer(101);
		System.out.println(a.hashCode());
	}
}
