/* LinkedHashMap - Insertion Order Preserved */

import java.util.*;

class LinkedHashMapDemo{

	public static void main(String[] args){

		LinkedHashMap hm = new LinkedHashMap();

		hm.put("Rahul","BMC");
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");

		System.out.println(hm);
	}

}


