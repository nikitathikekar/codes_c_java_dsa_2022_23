/* Difference between HashMap And WeakHashMap */

import java.util.*;
class Demo{

	String str=null;

	Demo(String str){
		this.str=str;
	}

	public String toString(){
		return str;
	}

	public void finalize(){
		System.out.println("Notify");
	}

}

class WeakHashMapDemo{

	public static void main(String[] args){
		Demo obj1 = new Demo("Core2Web");
		Demo obj2 = new Demo("Beincaps");
		Demo obj3 = new Demo("Incubator");
/*
		HashMap hm = new HashMap();

		hm.put(obj1,2016);
		hm.put(obj2,2019);
		hm.put(obj3,2023);
		System.out.println(hm);

		obj1=null;
		System.gc();
		System.out.println("In main");
*/
		WeakHashMap whm = new WeakHashMap();

		whm.put(obj1,2016);
		whm.put(obj2,2019);
		whm.put(obj3,2023);
		System.out.println(whm);

		obj1=null;
		System.gc();
		System.out.println("In main");
	}
}
