/* Duplicate key in IdentityHashMap */

import java.util.*;

class IdentityHashMapDemo{

	public static void main(String[] args){

		HashMap hm = new HashMap();

		/*hm.put(10,"Niki");
		hm.put(20,"Sam");
		hm.put(30,"Shubh");
		hm.put(10,"Dipi");   updated key value printed in this case 10=Dipi print instead of 10=Niki*/


		hm.put(new Integer(10),"Niki");
		hm.put(new Integer(20),"Sam");
		hm.put(new Integer(30),"Shubh");
		hm.put(new Integer(10),"Dipi"); //In this case updatd key value is printed same as above so we use IdentityHashMap
	
		System.out.println(hm);

		IdentityHashMap ihm = new IdentityHashMap();
		
		ihm.put(new Integer(100),"Niki");
		ihm.put(new Integer(200),"Sam");
		ihm.put(new Integer(300),"Shubh");
		ihm.put(new Integer(100),"Dipi"); 

		System.out.println(ihm);
		
		IdentityHashMap ihm2 = new IdentityHashMap();

		ihm2.put(50,"Ram");
		ihm2.put(60,"Sam");
		ihm2.put(70,"ZAm");
		ihm2.put(50,"Kam");//Integer Cache

		System.out.println(ihm2);
	
	}
}
