/* Duplicate key in Hashmap */

import java.util.*;

class DuplicateHashMapDemo{

	public static void main(String[] args){

		HashMap hm = new HashMap();

		hm.put("Ashish","Infosys");
		hm.put("Badhe","CarPro");
		hm.put("Ashish","Barclays");
		hm.put("Rahul","BMC");

		System.out.println(hm);
	}
}
