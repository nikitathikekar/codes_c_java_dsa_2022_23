
class Parent{

	Parent(){
		System.out.println(this);
		System.out.println("In Parent Constructor");
	}
	void Prop(){
		System.out.println("Flat, Car, Gold");
	}
}

class Child extends Parent{

	Child(){
		System.out.println(this);
		System.out.println("In Child Constructor");
	}
}

class Client{

	public static void main(String[] args){

		Parent obj1= new Parent();
		System.out.println(obj1);
		obj1.Prop();

		Child obj2 = new Child();
		System.out.println(obj2);
	}
}


