 
class Parent{

	int x=10;
	static int y=20;

	Parent(){
		System.out.println("Parent");
	}
	void access(){
		System.out.println(x);
		System.out.println(y);
	}
}
class child extends Parent{

	int x=100;
	static int y=200;

	child(){
		System.out.println("Child");
	}
	void access(){
		System.out.println(x);
		System.out.println(y);
		super.access();
	}
}
class client{
	public static void main(String[] args){
		child obj = new child();
		obj.access();
	}
}
