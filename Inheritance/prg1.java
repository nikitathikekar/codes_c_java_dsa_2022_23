/* Parent child Relation */

class Parent{
	Parent(){
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("In Child Constructor");
	}
}
class Client{
	public static void main(String[] args){
		Parent obj2 = new Parent();
		Child obj1 = new Child();
	}
}
