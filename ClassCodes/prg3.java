/* Integer Cache - Memory Management
 Boundary condition is from -128 to 127*/

class Demo3{

	public static void main(String[] args){

		int x=1000;
		int y=1000;
		Integer z =1000;
		//Integer n = new Integer(10);

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));

		//System.out.println(System.identityHashCode(n));
	}
}
