
class StringBufferDemo4{

	public static void main(String[] args){

		StringBuffer sb = new StringBuffer();
		sb.append("Core2Web");
		sb.append("Biencaps");

		System.out.println(sb);
		System.out.println(sb.capacity());

		sb.append("Incubator");
		System.out.println(sb);
		System.out.println(sb.capacity());
	}
}
