/*String Constant Pool*/

class Demo2{

	public static void main(String[] args){

		String str[] = {"Nikita","Nikita"};
	
		System.out.println(System.identityHashCode(str[0]));//Stringsin array is same so it generate same identity
		System.out.println(System.identityHashCode(str[1]));//Hash Code

		
		System.out.println(System.identityHashCode(args[0]));
		System.out.println(System.identityHashCode(args[1]));
//Here in cmd If we give same strings to array it will generate different identity hash code bcz Cmd Argumnets may call   new keyword so new object is create and cmd has dynamically or at runtime generating the array.
	}
}

