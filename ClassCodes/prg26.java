class StringDemo7{

	public static void main(String[] args){
		int x=10;
		String str1 = "Kanha";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	}
}
