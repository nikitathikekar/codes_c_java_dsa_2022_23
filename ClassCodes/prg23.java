
class StringDemo4{

	public static void main(String[] args){
		char ch='A';
		int x= 65;

		System.out.println(System.identityHashCode(ch));//internally call valueOf() method of Character class which create a new object when we access identityHashCode
		System.out.println(System.identityHashCode(x));
	}
}

