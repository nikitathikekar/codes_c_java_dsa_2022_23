/* We can declare jagged array in 2 ways*/

class JaggedArr3{

	public static void main(String[] args){

		int arr1[][] ={{1,2,3},{4,5},{7}};//Initialzr list
		int arr2[][] = new int[3][];

		arr2[0] = new int[]{1,2,3};
		arr2[1] = new int[] {4,5};
		arr2[2] = new int[]{7};

	}
}
