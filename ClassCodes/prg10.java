/* Command Line Argument*/

class  Cmd1{

	public static void main(String ...args){
//public static void main(String ... args) <-- We can also use this type of declaration called elipses or variable number of arguments.

		int arr[] = {10,20,30};
		
		for(int i=0;i<args.length;i++){
			System.out.println(args[i]);
		}
		System.out.println(args[0]+args[1]+args[2]);
	}
}
