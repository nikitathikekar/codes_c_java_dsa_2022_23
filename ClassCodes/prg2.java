/* Find the primitive data type that has no wrapper class - All the primitive data type in java has wrapper class*/

import java.io.*;
class Demo2{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int x = Integer.parseInt(br.readLine());
		float y = Float.parseFloat(br.readLine());
		double z = Double.parseDouble(br.readLine());
		byte p = Byte.parseByte(br.readLine());
		short m = Short.parseShort(br.readLine());

		//char n = Char.parseChar(br.readLine());
	}
}



