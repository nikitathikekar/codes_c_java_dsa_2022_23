/* Passing array to the method/function */

class PATFun1{

	static void fun(int arr[]){
		for(int x: arr){
			System.out.println(x);//10 20 30
		}
		arr[0]=50;
	}

	public static void main(String[] args){
		
		int arr[] = {10,20,30};
		for(int x:arr){
			System.out.println(x);//10 20 30
		}

		fun(arr);

		for(int x:arr){
			System.out.println(x);//50 20 30
		}
	}
}



