/* passing value to a function*/

class PVTFun{	

	void fun(int x,double y){
		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] args){	
		PVTFun obj = new PVTFun();
		
		System.out.println(obj.fun(10,50.4)); //error void type not allowed here bcz method fun has void return type
	}
}

