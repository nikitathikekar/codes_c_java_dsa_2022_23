/* String */

class StringDemo1{

	public static void main(String[] args){

		String str1 = "Core2Web";//SCP
		String str2 = new String("Core2Web");//Heap
		char str3[] = {'C','2','W'};//IntegerCache

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
