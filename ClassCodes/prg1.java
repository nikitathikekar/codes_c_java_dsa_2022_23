/*address of an Array*/

class Demo{

	public static void main(String[] args){

		int arr1[] ={100,200,300,400,500};
		short arr2[] ={10,20,30,40,50};
		byte arr3[] ={1,2,3,4,5};

		boolean arr4[] ={true,false};//if we are not entering any data in boolean array then bydefault it is false

		char arr5[] ={'A','B','C'};//In char by default space is there

		float arr6[]=new float[3];
		System.out.println(arr6[0]);//0.0
		
		System.out.println(arr1);
		System.out.println(arr2);
		System.out.println(arr3);
		System.out.println(arr4);
		System.out.println(arr5);

	}
}

