class StringDemo5{

	public static void main(String[] args){

		int arr1[] = {10,200,300};
		int arr2[] = {10,200,300};

		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr1[1]));
		System.out.println(System.identityHashCode(arr1[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));//int is a primitive data type so when we call iHC() then valueOf() function create new object each time when we accessed but when we used Integer Class instead of int object is created only once and valueOf() function is there but it doesn't call new so new object not create
		System.out.println(System.identityHashCode(arr2[2]));

		System.out.println(" When we used Integer Class Then");
		int arr3[] = {20,400,700};
		Integer arr4[] = {20,400,700};

		System.out.println(System.identityHashCode(arr3[0]));
		System.out.println(System.identityHashCode(arr3[1]));
		System.out.println(System.identityHashCode(arr3[2]));

		System.out.println(System.identityHashCode(arr4[0]));
		System.out.println(System.identityHashCode(arr4[1]));
		System.out.println(System.identityHashCode(arr4[2]));

		System.out.println(System.identityHashCode(arr4[0]));
		System.out.println(System.identityHashCode(arr4[1]));
		System.out.println(System.identityHashCode(arr4[2]));

	}
}
