/* Jagged Array*/

class JaggedArr{

	public static void main(String[] args){

		int arr[][] = {{10,20,30},{30,40},{50}};

		for(int x[]:arr){

			for(int y:x){

				System.out.print(y);
			}
			System.out.println();
		}
	}
}
