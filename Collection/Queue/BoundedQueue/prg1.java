/* Blocking Queue */

import java.util.concurrent.*;

class BlockingQueueDemo{

	public static void main(String[] args)throws InterruptedException{

		BlockingQueue bQueue = new ArrayBlockingQueue(3);

		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);

		System.out.println(bQueue);

		bQueue.put(40);//block honar bcz size 3 declare keliye it can be wait that other thread will come and delete element so it can put 40 on queue
		System.out.println(bQueue);
	}
}


