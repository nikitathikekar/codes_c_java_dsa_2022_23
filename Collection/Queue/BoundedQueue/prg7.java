/* Priority Blocking Queue */

import java.util.concurrent.*;
import java.util.*;

class Employee{

	int empId =0;
	String name;

	Employee(int empId,String name){
		this.empId = empId;
		this.name = name;
	}
	public String toString(){
		return empId+ ": " +name;
	}
}

class sortByName implements Comparator{
	
	public int compare(Object obj1,Object obj2){
		return ((Employee)obj1).name.compareTo(((Employee)obj2).name);
	}
}

class PriorityQueueDemo{

	public static void main(String[] args){

		PriorityBlockingQueue PBQueue = new PriorityBlockingQueue(4,new sortByName());
		
		PBQueue.offer(new Employee(1,"Nikita"));
		PBQueue.put(new Employee(21,"Shreya"));
		PBQueue.put(new Employee(15,"Sam"));
		PBQueue.put(new Employee(10,"Mrudula"));

		System.out.println(PBQueue);
	}
}

		

