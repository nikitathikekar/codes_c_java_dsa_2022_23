/* Priority Blocking Queue - 4 Constructor
 Priority Blocking Queue never blocks if we enter elements more than is capacity it added on the queue because it is depend on priority queue and priority queue is
unbounded. */

import java.util.concurrent.*;

class PriorityBlockingQueueDemo{

	public static void main(String[] args){

		PriorityBlockingQueue PBQueue = new PriorityBlockingQueue(3);

		PBQueue.offer(10);
		PBQueue.offer(20);
		PBQueue.offer(30);
		PBQueue.offer(10);
		PBQueue.offer(10);
		PBQueue.offer(10);
		PBQueue.offer(20);
		PBQueue.offer(30);
		PBQueue.offer(20);
		PBQueue.offer(30);
		PBQueue.offer(20);
		PBQueue.offer(30);

		System.out.println(PBQueue);

		PBQueue.offer(40);
		System.out.println(PBQueue);
		
		PBQueue.offer(50,5,TimeUnit.SECONDS);
		System.out.println(PBQueue);

	}
}
