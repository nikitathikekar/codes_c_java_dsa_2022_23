/* Blocking Queue */

import java.util.concurrent.*;

class BlockingQueueDemo{

	public static void main(String[] args){

		BlockingQueue bQueue = new ArrayBlockingQueue(3);

		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);

		System.out.println(bQueue);

		try{
			bQueue.put(40);//ithch block hoto catch mdhe jatch nhi but interrupted exception compile time exception ahe te compile krtana ch check hoil so 
				       //error nhi yenar
		}catch(InterruptedException ie){
			System.out.println(bQueue);
		}
	}
}


