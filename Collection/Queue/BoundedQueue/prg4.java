/* ArrayBlockingQueue Class- 3Constuctor*/

import java.util.concurrent.*;

class ArrayBlockingQueueDemo{

	public static void main(String[] args)throws InterruptedException{

		ArrayBlockingQueue ABQueue = new ArrayBlockingQueue(5,false);

		ABQueue.put(10);
		ABQueue.put(20);
		ABQueue.put(30);
		ABQueue.put(40);

		System.out.println(ABQueue);

		ABQueue.put(50);
		ABQueue.take();
		ABQueue.poll();
		System.out.println(ABQueue);
	}
}


