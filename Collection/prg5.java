import java.util.*;

class ArrayListDemo{

	public static void main(String[] args){
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
		al.add(new Integer(40));
		al.add(30);

		for(val obj:al){
			System.out.println(obj);
		}
	}
}
