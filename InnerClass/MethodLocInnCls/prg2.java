/*  Name of .class file
 function chya at function and tyachya at inner class chaltch nhi
 */

class Outer{

	void m1(){
		System("In outer m1");
		void m2(){
			System.out.println("In inner m2");
			class Inner{

			}
			Inner obj = new Inner();
		}
	}
}
class Client{
	public static void main(String[] args){
		Outer obj = new Outer();
		obj.m1();
		//obj.m1().m2();
	}
}
