
class Demo{

	int x=10;
	static int y=20;

	void fun(){
		System.out.println("in demo fun");
		System.out.println(x);
	}

}
class Client{

	public static void main(String[] args){

		Demo obj = new Demo(){
			int x=30;
			final static int y=40;
			void fun(){
				System.out.println("In Client$1 Fun");
				System.out.println(x);
				System.out.println(y);
				System.out.println(Demo.y);
				//System.out.println(obj.x);
			}

		};
		obj.fun();
	}
}

