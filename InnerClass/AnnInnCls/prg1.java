/* Annonymous inner class mdhe compiler name deto annonymous class la aplya code mdhe inner class ch name 'Client$1' as ahe ani Client$1 cha parent ahe Demo He pn compiler add krto bytecode check kela tr, mg inner class object create krto tevha direct new inner class  bnto mhnun yala one time use sathi use kela jato. Jevha parent chya sglya method override krayhya astil child class mdhe tevha annonymous class use krtat bacause jr ekhadi method parent mdhe ahe ani child mdhe nhiye tr parent mdhli method call kshi krnar jevha apn parent cha object bnvto 'Demo obj = new Demo()' compiler ith parent cha reference ani child cha object as krto'Demo obj = new Client$1' so child chya ch ethod call hotat parent chya nhi hot */

class Demo{

	void marry(){
		System.out.println("Kriti Sanon");
	}
}
class Client{
	public static void main(String[] args){
		Demo obj = new Demo(){
			void marry(){
				System.out.println("Disha Patni");
			}
		};
		obj.marry();
	}
}
