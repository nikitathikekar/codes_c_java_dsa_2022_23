class demo{
	demo(){
		System.out.println(this);
	}
	demo(demo obj){
		System.out.println(this);
		System.out.println(obj);

	}
}
class client{
	public static void main(String[] args){
		demo obj = new demo();
		System.out.println(obj);
		demo obj1 = new demo(obj);
		System.out.println(obj1);
	}
}
