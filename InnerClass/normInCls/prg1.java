/* Normal Inner Class Code*/
class Outer{

	class Inner{
		Inner(){
			System.out.println("In inner Constructor");
		}
		void fun2(){
			System.out.println("In Inner-fun2");
		}
	}
	void fun1(){
		System.out.println("In Outer- fun1");
	}
}
class Client{
	public static void main(String[] args){
	/*	Outer obj = new Outer();
		obj.fun1();

		Outer.Inner obj1 = obj.new Inner();
		obj1.fun2();*/

		Outer.Inner obj2 = new Outer().new Inner();
		obj2.fun2();
	}
}
