/* We can access all methods and variables of Outer Class from Inner Class */

class Outer{
	 int x=10;
	 static int y=20;

	 class Inner{
		 
		 void fun(){
			 System.out.println(x);
			 System.out.println(y);
			 fun2();
			 fun3();
		 }
	 }
	 void fun2(){
		 System.out.println("In Outer-fun2");
	 }
	 static void fun3(){
		 System.out.println("In static Outer fun3");
	 }
}
class Client{
	public static void main(String[] args){
		Outer.Inner obj1 = new Outer().new Inner();
		obj1.fun();
	}
}
