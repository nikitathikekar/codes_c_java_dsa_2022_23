/* Accessing Non Static variable/method from non-static method*/

class prg4{

	int x=10;
	static int y=20;
	void gun(){
		System.out.println("In gun x= " +x);
	}
	void fun(){
		System.out.println("In fun");
		System.out.println(x);
		System.out.println(y);
		gun();
	}
	public static void main(String args[]){
		prg4 obj = new prg4();

		obj.fun();
	}
}
