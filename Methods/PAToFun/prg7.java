/* Check Palindrome or not using method*/

import  java.util.Scanner;
class prg7{

	void Palin(int num){
		int rev=0;
		int temp=num;
		while(num!=0){
			int rem=num%10;
			rev = rem+rev*10;
			num=num/10;
		}
		if(temp==rev){
			System.out.println("Given no. is Palindrome");
		}else{
			System.out.println("Not a Palindrome");
		}

	}
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		prg7 obj = new prg7();

		System.out.println("Enter a number");
		int num = sc.nextInt();

		obj.Palin(num);
	}
}
