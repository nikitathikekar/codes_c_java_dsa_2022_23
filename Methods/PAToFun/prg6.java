/* Sum of digit using method*/
import java.util.Scanner;

class prg6{
	
	void SumOfDig(int a){
		int sum=0;
		while(a>0){
			int rem = a%10;
			sum= sum+rem;
			a/=10;
		}
		System.out.println("Sum Of Digit is: "+sum);

	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		prg6 obj = new prg6();
		System.out.println("Enter an integer Number");
		int a = sc.nextInt();
		
		obj.SumOfDig(a);
	}
}
