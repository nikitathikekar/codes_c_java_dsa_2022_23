/* Passing different types of data to functions */

class prg10{

	public static void main(String[] args){

		System.out.println("In main");
		prg10 obj = new prg10();
		obj.fun(10);
		obj.fun(10.5f);
	//	obj.fun(10.5);
	//	obj.fun(true);
		obj.fun('a');
	}
	void fun(float x){
		System.out.println(x);
	}
}
