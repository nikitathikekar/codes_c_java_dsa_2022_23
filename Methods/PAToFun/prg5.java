/* Passing Agument/values to a function or method*/
import java.util.Scanner;
class prg5{

	static void add(int a,int b){
		int ans = a+b;
		System.out.println("Addition is: "+ans);
	}
	static void sub(int x,int y){
		int ans= x-y;
		System.out.println("Subtraction is: "+ans);
	}
	static void mul(int x,int y){
		int ans = x*y;
		System.out.println("Multiplication is: "+ans);
	}
	static void div(int x,int y){
		int ans =x/y;
		System.out.println("Division is: "+ans);
	}
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Two Integer values");
		int a =sc.nextInt();
		int b =sc.nextInt();

		add(a,b);
		sub(a,b);
		mul(a,b);
		div(a,b);
	}
}
