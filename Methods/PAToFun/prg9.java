/* Function divide into three types function call, function header, function body
 Function header - return type, function name, function parameter*/

class prg9{

	public static void main(String[] args){
		prg9 obj = new prg9();
		obj.fun(10);
	}

	static void (int x){
		System.out.println(x);
	}
}

/* If NAme Of the function is not present error - Identifier Expected
 If return type of method not given error - invalid method declaration return type required*/

