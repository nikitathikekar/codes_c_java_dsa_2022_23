/* Access Non-Static variable/method from static context by creating an object*/
class prg3{
	int x=10;
	static int y=20;
	void fun(){
		System.out.println("In Fun");
	}
	static void gun(){
		System.out.println("In gun");
	}
	public static void main(String[] args){
		System.out.println("In main");
		prg3 obj = new prg3();
		System.out.println(obj.x);
		System.out.println(y);


		obj.fun();
		gun();
	}

}


		
