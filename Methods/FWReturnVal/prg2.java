/*If we want to catch Return value comes from functions nut that fun/method has voidreturn type then error- incompatible
types: void cannot be converted to int*/

class prg2{

	int fun(int x){

		return x+10;
	}
	public static void main(String[] args){
		prg2 obj = new prg2();

		int a = obj.fun(10);
		System.out.println(a);
	}
}
