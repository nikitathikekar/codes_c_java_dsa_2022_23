/* Function with return values */

/*class prg1{
	
	void fun(int x){
	
	}

	public static void main(String[] args){
		prg1 obj = new prg1();

		System.out.println(obj.fun(10));	

	}
}/* Error- void type not allowed here*/

class prg1{

	int fun(int x){
		return x+10;
	}
	public static void main(String[] args){
		prg1 obj = new prg1();

		System.out.println(obj.fun(10));
	}
}
