/* 
1	2	9
4	25	6
49	8	81
*/

class prg7{
	public static void main(String[] args){
		int var=1;
		for(int i=1; i<=3; i++){

			for(int j=1; j<=3; j++){

				if((i%2==1 && j%2==1) || (i%2==0 && j%2==0)){
					System.out.print(var*var+"    ");
				}else if((i%2==1 && j%2 == 0) || (i%2==0 && j%2==1)){
					System.out.print(var+"    ");
				}
				var++;
			}
			System.out.println();
		}
	}
}
