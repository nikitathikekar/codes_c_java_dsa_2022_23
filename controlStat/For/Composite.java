/* Composite Number- Not a prime number A number whose factor is greater than 2
 but 0 and 1 are the numbers which is not prime nor composite
 4,6,8,9,10..etc*/

import java.util.Scanner;

class Composite{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int num = sc.nextInt();

		int count=0;
		if(num==0 || num==1){
			System.out.println(num+ " Is not a prime nor composite");
		}else{

			for(int i=1; i<=num; i++){
	
				if(num % i==0){
					count++;
				}
			}
			if(count>2){
				System.out.println(num+" Is composite number");
			}else{
				System.out.println(num+" Is not a Composite number");
			}
		}

	}
}

