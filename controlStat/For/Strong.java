/* A Strong number is a number where the sum of the factorial of the digit is equal to the number itself.
 145 = 1! + 4! + 5!*/

import java.util.Scanner;
class Strong{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");

		int num=sc.nextInt();
		int sum=0,temp=num;

		while(num!=0){
			int rem=num%10;
			int fact=1;
			for(int i=rem; i>=1; i--){
				fact=fact *i;
			}
			sum=sum+fact;
			num=num/10;
		}
		if(temp==sum){
			System.out.println(temp+ " Is a Strong Number");
		}else{
			System.out.println(temp+ " Is not a Strong Number");
		}
	}
}


