/* Take N as input print odd integers from 1 to N */

import java.util.Scanner;
class Odd{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");

		int num = sc.nextInt();

		for(int i=1; i<=num; i+=2){
			System.out.println(i);
		}
	}
}
		

