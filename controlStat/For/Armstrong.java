/* Armstrong Number is one whose sum of digit raised to power of total no. of digit is equal to that number
 0
 1
 153 = 1^3 + 5^3 +3^3
 1634 = 1^4+ 6^4 + 3^4 + 4^4
 8208 = 8^4 + 2^4 + 0^4 +8^4
 */
import java.util.Scanner;
class Armstrong{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int N =sc.nextInt();
		int count=0,temp1=N,sum=0,temp2=N;
		
		while(N!=0){
			count++;
			N=N/10;
		}
		while(temp1!=0){
			int rem = temp1%10;
			int mult=1;

			for(int i=1; i<=count; i++){
				mult= rem*mult;
			}
			sum=sum+mult;
			temp1=temp1/10;
		}
		if(temp2 == sum){
			System.out.println("Armstrong No");
		}else{
			System.out.println("Not an Armstrong No");
		}
	}
}


			





