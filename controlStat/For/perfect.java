/* Take n as input. Print whether N is perfect number or not- perfect number- when sum factor of given number without 
including itself is that number.eg- 6 = 1+2+3 =6
28 = 1+2+4+7+14 =28
496
8128
*/
import java.util.Scanner;
class perfect{
	 
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int num =sc.nextInt();
		int sum=0;

		for(int i=1; i<num; i++){

			if(num%i==0){
				sum=sum+i;
			}
		}
		if(sum==num){
			System.out.println(num+" Is a Perfect Number");
		}else{
			System.out.println(num+" Not a perfect Number");
		}
	}
}
