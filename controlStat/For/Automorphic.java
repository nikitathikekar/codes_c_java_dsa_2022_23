/* Automorphic no. is a number whose sqr ends with a given integer number
 eg.- 5 = 25, 5==5
 6 = 36,6==6,
 25= 625, 25==25
 76 = 5776, 76==76
376
 */

import java.util.Scanner;

class Automorphic{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int num = sc.nextInt();
		int sqr=num*num,temp1=num,temp2=num,count=0,flag=0;

		while(num!=0){
			count++;
			num=num/10;
		}

		for(int i=1;i<=count;i++){
			
			if(temp1%10 == sqr%10){
				flag++;
			}
			temp1=temp1/10;
			sqr=sqr/10;
		}
		if(count==flag){
			System.out.println(temp2+ " Is Automorphic Number");
		}else{
			System.out.println(temp2+ " Not an Automorphic number");
		}
	}
}



