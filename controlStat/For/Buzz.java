/* A number is a buzz number if it is divisible by 7 or ends with 7 
 42,107,147*/

import java.util.Scanner;

class Buzz{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number");
		int num = sc.nextInt();
		
		if((num %7==0) ||(num%10==7)){
			System.out.println(num+ " Is a Buzz Number");
		}else{
			System.out.println(num+ " Not a Buzz Number");
		}
	}
}

