/*
10
9	8
7	6	5
4	3	2	1
*/

class prg3{

	public static void main(String[] main){
		int row=4;
		int num=(row*(row-1))/2+row;

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				System.out.print(num+"	");
				num--;
			}
			System.out.println();
		}
	}
}

