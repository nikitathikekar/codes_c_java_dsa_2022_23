/* Position of default- Default has less priority than other cases */

class SwitchDemo2{

	public static void main(String[] args){
		int x=5;

		switch(x){
			default:
				System.out.println("No match");
				break;	
			case 1:
				System.out.println("1");
				break;
			case 2: 
				System.out.println("2");
				break;
			case 3:
				System.out.println("3");
				break;
			case 4:
				System.out.println("4");
				break;
		}
		System.out.println("After Switch");
	}
}
