/* In java int,byte,short,char is allowed to pass in switch till the version 1.6
 but from java 1.7 int,char,byte,short,string,enum are allowed to pass in switch*/

class SwitchDemo7{

	public static void main(String[] args){
		String str="Mon";

		switch(str){

			case "Mon":
				System.out.println("Monday");
				break;
			case "Tue":
				System.out.println("Tuesday");
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After Switch");
	}
}
