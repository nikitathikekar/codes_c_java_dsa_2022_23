/* Switch using ASCII values*/

class SwitchDemo5{

	public static void main(String[] args){

		int ch=65;
		switch(ch){
			case 'A':
				System.out.println("char-A");
				break;
			case 65:
				System.out.println("num-A");
				break;
			case 'B':
				System.out.println("char-B");
				break;
			case 66:
				System.out.println("num-B");
				break;
			case 'C':
				System.out.println("char-C");
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After Switch");
	}
}

