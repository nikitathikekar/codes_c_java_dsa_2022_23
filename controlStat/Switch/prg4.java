/* Duplicate cases with expression*/

class SwitchDemo4{
	public static void main(String[] args){
		int x=2;
		switch(x){
			case 1:
				System.out.println("1");
				break;
			case 1+1:
				System.out.println("2");
				break;
			case 3:
				System.out.println("3");
				break;
			case 5:
				System.out.println("2");
				break;
			case 4:
				System.out.println("4");
				break;
			default:
				System.out.println("No match");
				break;
		}
		System.out.println("After Switch");
	}
}
