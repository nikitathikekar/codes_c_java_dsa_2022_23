/* Duplicate Cases Are Not Allowed*/

class SwitchDemo3{

	public static void main(String[] args){
		int x=5;

		switch(x){
			case 1:
				System.out.println("1");
				break;
			case 2:
				System.out.println("2");
				break;
			case 3:
				System.out.println("3");
				break;
			case 4:
				System.out.println("4");
				break;
			case 5:
				System.out.println("First-5");
				break;
			case 5:
				System.out.println("Second-5");
				break;
			case 2:
				System.out.println("Sec-2");
				break;
			default:
				System.out.println("no match");
				break;
		}
		System.out.println("After Switch");
	}
}


