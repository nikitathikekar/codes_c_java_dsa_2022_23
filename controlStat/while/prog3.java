class mul{
	public static void main(String[] args){
		int num=104;
		int mul=1;
		while(num!=0){
			int rem=num%10;
			
			if(rem==0){
				mul=0;
				break;
			}else{
				mul=mul*rem;
			}
			num=num/10;
		}
		System.out.println("Product is "+mul);
	}
}
