/* WAP to print the sum of all even numbers & multiplication of odd number between 1 to 10*/

class prog6{

	public static void main(String[] args){
		int i=1,sum=0,mul=1;

		while(i<=10){

			if(i%2==0){
				sum=sum+i;
			}else{
				mul=mul*i;
			}
			i++;
		}
		System.out.println("Sum of all Even Numbers "+sum);
		System.out.println("Multiplication of odd Numbers "+mul);
	}
}

