/* WAP to count the odd digits of a given number */

class OdDigit{

	public static void main(String[] args){

		int num= 942111423;
		int count =0;

		while(num!=0){
			int rem=num%10;

			if(rem % 2 !=0){
				count++;
			}
			num= num/10;
		}

		System.out.println("Count of odd number is "+count);
	}
}

