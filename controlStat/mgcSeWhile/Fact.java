/* Calculate factorial of a given Number */

class Fact{

	public static void main(String[] args){
		int fact=1;
		int x=5,i=1;
		if(x==0){
			fact=1;
		}else{
			while(i<=x){
				fact=fact*i;
				i++;
			}
		}
		System.out.println("Factorial of 5 is "+fact);
	}
}
