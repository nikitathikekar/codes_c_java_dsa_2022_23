/* Count Digits of a given number */

class CntDigit{
	
	public static void main(String[] args){

		int num = 942111423,count=0;

		while(num!=0){
			count++;
			num=num/10;
		}
		System.out.println("Count of Digit is "+count);
	}
}
			
