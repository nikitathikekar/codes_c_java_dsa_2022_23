/* WAP to print the Square of even digits in a given number*/

class SqrEvenDig{

	public static void main(String[] args){
		int num= 942111423;

		while(num!=0){
			int rem=num%10;

			if(rem%2==0){
				System.out.println("Square Of Even Digits in a given number "+(rem*rem));
			}
			num=num/10;
		}
	}
}
