/*Nested Switch*/

class NestSwitch{

	public static void main(String[] args){
		String str = "Non Veg";
		System.out.println("Before Switch");
		switch(str){
			case "Veg":
				{
					String str1="Main Course";
					
					switch(str1){
						case "Starter":
							System.out.println("Starter of Veg");
							break;
						case "Main Course":
							System.out.println("Main Course of Veg");
							break;
					}
				}
				break;
			case "Non Veg":
				{					
					String str1="Starter";
							
					switch(str1){
						case "Starter":
							System.out.println("Starter of NonVeg");
							break;
						case "Main Course":
							System.out.println("Main Course of NonVeg");
							break;

					}
				}
				break;
		}
		System.out.println("After Switch");
	}
}

		
							

