/*
F
E	1
D	2	E
C	3	D	4
B	5	C	6	D
A	7	B	8	C	9
*/

class prg7{

	public static void main(String[] args){
		int row=6,num1=1;

		for(int i=1; i<=row; i++){
			int num2= 71-i;
			for(int j=1; j<=i; j++){
				
				if(j%2!=0){
					System.out.print((char)num2+"	");
					num2++;
				}else{
					System.out.print(num1+"	");
					num1++;
				}
			}
			System.out.println();
		}
	}
}
