/*
1
8	9
9	64	25
64	25	216	49
*/

class prg10{

	public static void main(String[] args){
		int row=4;

		for(int i=1; i<=row; i++){
			int num=i;
			for(int j=1; j<=i; j++){

				if((i+j)%2==1){
					System.out.print(num*num*num+"    ");
				}else{
					System.out.print(num*num+"    ");
				}
				num++;
			}
			System.out.println();
		}
	}
}
