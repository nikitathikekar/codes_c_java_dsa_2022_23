/*
1	2	3	4
2	3	4
3	4
4

*/

class prg4{

	public static void main(String[] args){

		int row=4;
		for(int i=1; i<=row; i++){
			int num=i;

			for(int j=i; j<=row; j++){
				System.out.print(num+"	");
				num++;
			}
			System.out.println();
		}
	}
}

