/* Runtime Exception- 3)Number Format Excepption- Handle Exception Using Try CAtch*/

import java.io.*;
class Demo{
	void m1()throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int data=0;
	        try{
			data= Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){
			obj.printStackTrace();
			System.out.println(data);
		}

	}
	public static void main(String[] args)throws IOException{
		Demo obj = new Demo();
		obj.m1();
	}
}
