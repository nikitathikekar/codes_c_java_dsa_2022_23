/* Try Catch */

class Arith{

	public static void main(String[] args){

		System.out.println("Start main");

		try{
			System.out.println(10/0);
		}catch(ArithmeticException obj){
			System.out.println("Divided by zero ");
		}

		System.out.println("End MAin");
	}
}
