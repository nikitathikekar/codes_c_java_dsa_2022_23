/* Two types Of Exception Handling - 1)Compile Time Exception
 2) runtime Exception
 */

//Compile Time Exception
import java.io.*;
class Demo{

	public static void main(String[] args){//throws IOException{
		
		BufferedReader br = new BufferedReader (new InputStreamReader(System.in));

		String str = br.readLine();
		System.out.println(str);


	}
}
