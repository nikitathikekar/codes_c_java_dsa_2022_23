
/* Try catch Finally - finally ha block compulsory execute hoto jri catch mdhe match sapto kinva n sapto */

class Demo{
	void m1(){
		System.out.println("In m1");
	}
	void m2(){
		System.out.println("In m2");
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.m1();
		
		obj = null;
		try{
			obj.m2();	
		/*}catch(NullPointerException e){
			System.out.println("Exception Caught");*/
		}catch(ArithmeticException e){
			System.out.println("EC In 2");
		}finally{
			System.out.println("Connection Closed");
		}

		System.out.println("end Main");

	}
}
