/* User Defined Exception - throw */

import java.util.Scanner;

class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String str){
		super(str);
	}
}

class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String str){
		super(str);
	}
}

class ArrayDemo{

	public static void main(String[] args){

		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Array Elements");
		System.out.println("0 < elements < 100");

		for(int i=0; i<arr.length; i++){

			int data = sc.nextInt();
			if(data<0){
				throw new DataUnderFlowException("Element is less than 0");
			}
			if(data>100){
				throw new DataOverFlowException("Element is greater than 100");
			}

			arr[i] = data;
		}
	}
}

