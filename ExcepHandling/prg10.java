/* One Try With Multiple Catch */

class Demo{
	void m1(){
		System.out.println("In m1");
	}

	public static void main(String[] args){
		Demo obj = new Demo();
		obj.m1();
		obj = null;

		try{
			obj.m1();
		}catch(NullPointerException obj2){
			System.out.println("Exception caught");
		}catch(RuntimeException obj2){
			System.out.println("In parent");
		}
		System.out.println("End MAin");
	}
}
