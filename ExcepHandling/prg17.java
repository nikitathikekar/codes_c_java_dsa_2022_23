

import java.io.*;

class SpecialCharNotFoundException extends InterruptedException{
	SpecialCharNotFoundException(String str){
		super(str);
	}
}

class PasswordDemo{

	public static void main(String[] args)throws IOException,InterruptedException{
		System.out.println("Enter a Password which include Characters,Special Characters");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String passWord = br.readLine();
		char[] str = passWord.toCharArray();
		int flag=0;

		for(int i=0; i<str.length; i++){
			if(str[i] == '@'){	
				flag=1;

			}
		}
		
		if(flag==0){
			throw new SpecialCharNotFoundException("Please Enter one Special Character");
		}else{
			System.out.println("Password : "+passWord);
		}	

	}
}
