/* Compile Time Exeption - Interrupted Exception */

class Interrupted{
	public static void main(String[] args){//throws InterruptedException{

		for(int i=0; i<10;i++){
			System.out.println("In Loop");
			try{
				Thread.sleep(500);
			}catch(InterruptedException e){
				System.out.println("Exception Caught");
			}
		}
		System.out.println("End main");
	}
}
