/* How to print the name of the thread */


class MyThread extends Thread{
	public void run(){
		System.out.println(getName());
	}
}

class ThreadDemo{

	public static void main(String[] args){
		MyThread obj = new MyThread();
		obj.start();
		System.out.println(obj.getName());

		System.out.println("Main: "+Thread.currentThread().getName());
	}
}
