/*Thread Group Create using Thread Class */

class MyThread extends Thread{
	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{

	public static void main(String[] args){
		ThreadGroup pThreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread(pThreadGP, "Maharashtra");
		MyThread obj2 = new MyThread(pThreadGP, "Goa");
		MyThread obj3 = new MyThread(pThreadGP, "Nashik");

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP,"Pakistan");

		MyThread obj4 = new MyThread(cThreadGP1, "Karachi");
		MyThread obj5 = new MyThread(cThreadGP1,"Lahore");

		obj4.start();
		obj5.start();

		ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP, "Bangladesh");
		MyThread obj6 = new MyThread(cThreadGP2,"Dhaka");
		MyThread obj7 = new MyThread(cThreadGP2,"Mirpur");

		obj6.start();
		obj7.start();

	}
}
