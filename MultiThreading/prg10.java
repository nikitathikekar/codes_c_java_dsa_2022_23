/* Join is apply to main method */

class MyThread extends Thread{
	static Thread nmMain =null;

	public void run(){
		try{
			nmMain.join();
		}catch(InterruptedException ie){

		}
		for(int i=0; i<10; i++){
			System.out.print("Run");
		}
	}
}

class ThreadDemo{

	public static void main(String[] args)throws InterruptedException{
		MyThread.nmMain = Thread.currentThread();
		MyThread obj = new MyThread();
		obj.start();
		obj.join();

		for(int i=0; i<500; i++){
			System.out.print("Main");
		}

	}
}
