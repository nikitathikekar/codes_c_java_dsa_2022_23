/* Creating Child Thread Using Runnable Interface */

class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread().getName());
		for(int i=0; i<10; i++){
			System.out.println("In Run");
		}
	}
}

class ThreadDemo{

	public static void main(String[] args){
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);
		t.start();

		System.out.println(Thread.currentThread().getName());

	}
}
