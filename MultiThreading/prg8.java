/* Concurrency Methods in Thread Class - 
 sleep();
 */

class MyThread extends Thread{

/*	MyThread(String name){
		super(name);
	}*/
	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		System.out.println(Thread.currentThread());
		MyThread obj = new MyThread();
		obj.start();
		Thread.sleep(100);

		Thread.currentThread().setName("Core2Web");
		System.out.println(Thread.currentThread());

	/*	MyThread obj2 = new MyThread("Incubator");
		obj2.start();*/
	}
}
