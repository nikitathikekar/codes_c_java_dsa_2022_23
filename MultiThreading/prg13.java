/*Change the name of Thread*/

class MyThread extends Thread{
	MyThread(String str){
		super(str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}

}

class ThreadDemo{
	public static void main(String[] args){
		MyThread obj1 = new MyThread("PQR");
		obj1.start();

		MyThread obj2 = new MyThread("XYZ");
		obj2.start();

	}
}
