/* Join()- There are three methods of join() -1)join()-with no argument
 2) join(long milliSec);
 3) join(long milliSec, int nanoSec)
 */

class MyThread extends Thread{
	public void run(){
		for(int i=0; i<10; i++){
			System.out.println("In run");
		}
	}
}

class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		MyThread obj = new MyThread();
		obj.start();
		obj.join();

		for(int i=0; i<10; i++){
			System.out.println("In main");
		}
	}
}
