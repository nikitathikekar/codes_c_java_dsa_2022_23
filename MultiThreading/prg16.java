:wq:wqead Group Creation Using Runnable Interface */


class MyThread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());

	}
}

class ThreadDemo{
	public static void main(String[] args){
		ThreadGroup pThreadGP = new ThreadGroup("AWS");
		
		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP, obj1, "EC2");
		Thread t2 = new Thread(pThreadGP, obj2, "Elastic Glacier");

		t1.start();
		t2.start();

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP, "Azure");
		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(cThreadGP1,obj3, "EWS");
		Thread t4 = new Thread(cThreadGP1,obj4, "EC2 - Azure");

		t3.start();
		t4.start();

		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}
}
