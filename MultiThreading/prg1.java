/* Creating Thread Using Thread Class 
 In Thread Class Total 10 constructor*/

class MyThread extends Thread{

	public void run(){
		System.out.println("Run: "+Thread.currentThread());
		
		for(int i=0; i<10; i++){
			System.out.println("In Run");
		}
	}
}

class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		System.out.println("Main: "+Thread.currentThread());

		MyThread obj = new MyThread();
		obj.start();
		Thread.sleep(1000);

		for(int i=0; i<10; i++){
			System.out.println("In Main");
		}
	}
}
