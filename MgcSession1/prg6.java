/*WAP and take 2 char if these characters are equal then print them as it is but if they are unequal then print their difference
i/p - a  p
o/p - the difference between a & p is 15
*/
import java.io.*;
class Demo6{
	public static void main(String[] main)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter first character");
		char ch1 =br.readLine().charAt(0);

		System.out.println("Enter second character");
		char ch2 =br.readLine().charAt(0);

		if(ch1==ch2){
			System.out.println(ch1+" and "+ch2+" are Equal");
		}else{
			int diff=(int)ch2-ch1;
			System.out.println("The diff between "+ch1+" and "+ch2+" are:"+diff);
		}
	}
}


