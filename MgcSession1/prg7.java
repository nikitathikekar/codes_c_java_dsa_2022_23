/*
O
14	13
L	K	J
9	8	7	6
E	D	C	B	A

10
I	H
7	6	5
D	C	B	A

*/
import java.io.*;
class Demo7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Row");
		int row = Integer.parseInt(br.readLine());

		int num1= (row*(row+1))/2;
		int ch = 64+(row*(row+1))/2;

		for(int i=1; i<=row; i++){

			for(int j=1; j<=i; j++){

				if(row%2==0){
					if(i%2!=0){
						System.out.print(num1+"  ");
					}else{
						System.out.print((char)ch+"  ");
					}
				}else{
					if(i%2!=0){
						System.out.print((char)ch+"  ");
					}else{
						System.out.print(num1+"  ");
					}
				}
				num1--;
				ch--;
			}
			System.out.println();
		}
	}
}





