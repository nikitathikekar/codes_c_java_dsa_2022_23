/* WAP to print even no. in reverse order and odd numbers in a standard way within a range take start and end from user
 start-2
 end-9
 o/p- 8 6 4 2
       3 5 7 9
*/

import java.io.*;
class Demo4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter starting no");
		int num1 = Integer.parseInt(br.readLine());
		System.out.println("Enter ending no");
		int num2 = Integer.parseInt(br.readLine());

		for(int i=num1; i<=num2; i++){
			if(i%2!=0){
				System.out.print(i+"	");
			}
		}
		System.out.println();
		for(int i=num2; i>=num1; i--){
			if(i%2==0){
				System.out.print(i+"	");
			}
		}
	}
}
		


