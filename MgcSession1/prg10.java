/* WAP to print series of prime nums from entered range (Take start and end no. from user)
I/p- starting no-10
     ending no-100
o/p- 11,13,17.......89,97
*/

import java.io.*;
class Demo10{
	public static void main(String[] main)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Starting no");
		int num1 = Integer.parseInt(br.readLine());

		System.out.println("Enter ending no");
		int num2 = Integer.parseInt(br.readLine());

		for(int i=num1; i<=num2; i++){

			int count=0;
			for(int j=1;j<=i;j++){

				if(i%j==0){
					count++;
				}
			}
			if(count==2){
				System.out.println(i);
			}
		}
	}
}


