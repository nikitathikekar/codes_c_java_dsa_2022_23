/*
5	4	3	2	1
8	6	4	2
9	6	3
8	4
5
*/
import java.io.*;
class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(br.readLine());
		
		for(int i=1;i<=row;i++){
			int num=row-i+1;

			for(int j=i;j<=row;j++){
				
				System.out.print(num*i+"	");
				num--;
			}
			System.out.println();
		}
	}
}


