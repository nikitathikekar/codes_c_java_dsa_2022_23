/*
D4  C3  B2  A1
A1  B2  C3  D4
D4  C3  B2  A1
A1  B2  C3  D4
*/
import java.io.*;
class Demo1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Row");
		int row = Integer.parseInt(br.readLine());
		
		for(int i=1; i<=row; i++){
			int num=0;
			int ch=64;
			for(int j=1; j<=row; j++){
				
				if(i%2!=0){
					System.out.print((char)(ch+row));
					System.out.print(num+row+"   ");
					ch--;
					num--;
				}else{
					System.out.print((char)(ch+1));
					System.out.print(num+1+"   ");
					ch++;
					num++;
				}
			}
			System.out.println();
		}
	}
}

					

