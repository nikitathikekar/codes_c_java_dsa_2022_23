/*
$
@	@
&	&	&
#	#	#	#
$	$	$	$	$
@	@	@	@	@	@
&	&	&	&	&	&	&
#	#	#	#	#	#	#	#
*/

import java.io.*;
class Demo8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter no of rows");
		int rows = Integer.parseInt(br.readLine());
		char []arr={'$','@','&','#'};
		int num=0;
		for(int i=1; i<=rows; i++){

			for(int j=1; j<=i; j++){
				System.out.print(arr[num]+"	");
				
			}
			if(num == arr.length-1){
				num=-1;
			}
			num++;
			System.out.println();
		}
	}
}

