/* WAP to take a no. as input and print the add of factorials of each digit from that no. 
i/p- 1234
Add of fact of each digit from 1234 = 33
*/

import java.io.*;
class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter a Number");
		int num = Integer.parseInt(br.readLine());
		int sum=0;

		while(num!=0){
			int rem = num%10;
			int fact=1;
			for(int i=rem; i>=1; i--){
				fact=fact*i;
			}
			sum=sum+fact;
			num=num/10;
		}
		System.out.println(sum);
	}
}
