/* Real Time Example of Overriding */

class Parent{
	Parent(){
		System.out.println("Parent Constructor");
	}
	void prop(){
		System.out.println("Home, Car, Gold");
	}
	void marry(){
		System.out.println("Deepika Padukon");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("Child Constructor");
	}
	void marry(){
		System.out.println("Katrina Kaif");
	}
}
class client{
	public static void main(String[] args){
		Parent p = new Child();
		p.prop();
		p.marry();
	}
}
