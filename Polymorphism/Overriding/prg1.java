/* return type in Overriding*/

class Parent{
	char fun(){
		System.out.println("In parent fun");
		return 'A';
	}
}
class Child extends Parent{
	int fun(){
		System.out.println("In child fun");
		return 10;
	}
}
class Client{
	public static void main(String[] args){
		Parent obj = new Child();
		obj.fun();
	}
}
