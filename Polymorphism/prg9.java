/* Real Time Example Of Overriding */

class Match{
	void matchType(){
		System.out.println("OneDay,T20,Test");
	}
}
class IPLMatch extends Match{
	void matchType(){
		System.out.println("T20");
	}
}
class testMatch extends Match{
	void matchType(){
		System.out.println("Test");
	}
}
class Client{
	public static void main(String[] args){
		Match obj1 = new IPLMatch();
		obj1.matchType();

		Match obj2 = new testMatch();
		obj2.matchType();
	}
}

