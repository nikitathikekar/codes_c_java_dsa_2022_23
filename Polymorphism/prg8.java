
/* Real Time Example of Method Overloading */

class IPL{

	void matchInfo(String team1, String team2){
		System.out.println(team1 + " Vs "+ team2);
	}
	void matchInfo(String team1, String team2, String Venue){
		System.out.println(team1+" Vs "+ team2);
		System.out.println("Venue = "+Venue);
	}
}
class Client{
	public static void main(String[] args){
		IPL ipl2023 = new IPL();
		ipl2023.matchInfo("CSK","GT");
		ipl2023.matchInfo("CSK","GT","NMSA");
	}
}

