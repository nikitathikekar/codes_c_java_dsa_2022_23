class Demo1{

	public static void main(String[] args){
		int a[];//error in c not java
		//int a[5];//error in java not in c
		//int arr0[] = new int[];//array dimension is missing
		int arr[] = new int[5];//new keyword assign space in heap section
		int arr2[]= new int[]{10,20,30,40,50};
		int arr3[]={10,20,30,40,50};
		System.out.println(arr[0]);//by default if data type of array is int and array is empty then it assigns 0 by default
	        float b[]=new float[5];	
		System.out.println(b[0]);//float asign 0.0 bydefault

		char c[] = new char[5];
		System.out.println(c[0]);//char does not asign anything if array is empty but it allocate space and space will print
		System.out.println(c[1]);
		System.out.println(c[2]);

		double d[] =new double[5];
		System.out.println(d[0]);//0.0 bydefault
		
		int []nik =new int[]{1,2,3,4,5};
		System.out.println(nik[1]);
		System.out.println(nik[2]);


	}
}
