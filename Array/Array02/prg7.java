/* WAP to find common elements between 2 arrays 
 I/p -Enter first Array = 1 2 3 5
      Enter Second Array = 2 1 9 8
 O/p - Common Elements:
       1
       2
 */

import java.io.*;
class ArrayDemo7{

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of first Array");
		int size1 = Integer.parseInt(br.readLine());

		System.out.println("Enter size of second Array");
		int size2 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];

		System.out.println("Enter Elements for first Array");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Elements for second Array");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Common Elements: ");
		for(int i=0; i<arr1.length; i++){
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] == arr2[j]){
					System.out.println(arr1[i]);
					break;
				}
			}
		}
	}
}


