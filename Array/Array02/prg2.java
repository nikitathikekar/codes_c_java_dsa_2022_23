/*WAP to find the number of even and odd integers in a given array of integer*/

import java.io.*;
class ArrayDemo2{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());
		int evenCount=0,oddCount=0;

		int arr[] = new int[size];
		System.out.println("Enter Elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2==0){
				evenCount++;
			}else{
				oddCount++;
			}
		}
		System.out.println("Number of Even Integer: "+evenCount);
		System.out.println("Number of Odd Integer: "+oddCount);
	}
}

