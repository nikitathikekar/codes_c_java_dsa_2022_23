/* WAP to take size of array from user and also take integer element from user find the minimum element from the array
*/

import java.util.*;
class ArrayDemo5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Size Of an Array");
		int size = sc.nextInt();

		int arr[]=new int[size];
		int min=arr[0];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
			if(min>arr[i])
				min = arr[i];
		}
		System.out.println("Min Element "+min);
	}
}
