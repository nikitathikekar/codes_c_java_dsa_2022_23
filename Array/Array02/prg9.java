/* WAP to merge two given arrays*/

import java.util.*;
class ArrayDemo9{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array1");
		int size1 = sc.nextInt();
		int arr1[] = new int[size1];

		System.out.println("Enter size Of Array2");
		int size2 = sc.nextInt();
		int arr2[] = new int[size2];
		int arr3[] = new int[size1+size2];

		System.out.println("Enter elements of first array");
		for(int i=0; i<arr1.length; i++){
			arr1[i]= sc.nextInt();
			arr3[i] = arr1[i];
		}
		System.out.println("Enter elements of Second array");
		for(int i=0; i<arr2.length; i++){
			arr2[i]= sc.nextInt();
			arr3[i+size1]=arr2[i];
		}
		System.out.println("Merged Array: ");
		for(int i=0; i<arr3.length; i++){
			System.out.println(arr3[i]);
		}
	}
}






