/* WAP to print the elements whose addition of digit is even */

import java.util.*;
class ArrayDemo10{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("enter size of array");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements of array");

		for(int i=0; i<arr.length; i++){
			arr[i]= sc.nextInt();
		}
		System.out.println("Elements whose addition of digit is even");
		for(int i=0; i<arr.length; i++){
			int sum=0;
			int temp=arr[i];
			while(arr[i]!=0){
				int rem = arr[i]%10;
				sum = sum + rem;
				arr[i] = arr[i]/10;
			}
			if(sum % 2==0){
				System.out.println(temp);
			}
		}
	}
}

