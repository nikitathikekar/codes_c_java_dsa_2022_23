/*  WAP to take size of array from user and also take integer elements from user find the maximum element from the array*/

import java.io.*;
class ArrayDemo6{

	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size Of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int max =arr[0];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(max < arr[i])
				max = arr[i];
		}
		System.out.println("Max Element "+max);
	}
}
