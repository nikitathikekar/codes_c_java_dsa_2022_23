/* WAP to search a specific element from an array and return its index*/

import java.io.*;
class ArrayDemo4{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo4 obj = new ArrayDemo4();
		int flag=0;

		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());
		System.out.println("Enter Element You want to Search");
		int search = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int i;
		for(i=0; i<arr.length; i++){
			if(search == arr[i]){
				flag =1;
				break;
			}
		}
		if(flag == 1)
			System.out.println(i);
		else
			System.out.println(-1);
	}
		
}
