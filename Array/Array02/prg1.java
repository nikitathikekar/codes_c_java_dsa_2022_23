/* WAP to create an array of 'n' integer elements where 'n' should be taken from user. Insert the values from users and 
find sum of all elements in the array*/

import java.io.*;
class ArrayDemo1{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an array");
		int size = Integer.parseInt(br.readLine());
		int sum=0;

		int arr[] = new int[size];
		System.out.println("Enter Elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			sum = sum + arr[i];
		}
		System.out.println("Sum of the array elements "+sum);
	}
}

