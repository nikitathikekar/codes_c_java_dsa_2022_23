/* WAP to find the sum of even & odd no. in an Array.Display the sum value*/

import java.io.*;
class ArrayDemo3{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());
		int sumEven=0, sumOdd=0;

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());

			if(arr[i]%2==0)
				sumEven = sumEven+arr[i];
			else
				sumOdd = sumOdd+arr[i];

		}
		System.out.println("Sum of even Number "+sumEven);
		System.out.println("Sum of Odd Numbers "+sumOdd);
	}
}


