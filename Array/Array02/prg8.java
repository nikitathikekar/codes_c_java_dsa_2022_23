/* WAP to find the uncommon elements between two arrays
 I/p - Enter First Array - 1 2 3 5
       Enter Second Array- 2 1 9 8
 O/p - Uncommon Elements- 3 5 9 8
 */

import java.util.*;
class ArrayDemo8{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of first array");
		int size1 =  sc.nextInt();
		System.out.println("Enter size of second array");
		int size2 = sc.nextInt();

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];
		System.out.println("Enter Elements of first Array");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter elements of second array");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = sc.nextInt();
		}
		
		System.out.println("Uncommon Elements: ");
		for(int i=0; i<arr1.length; i++){
			int flag =0;
			for(int j=0; j<arr2.length; j++){
				if(arr1[i]==arr2[j]){
					flag =1;
				}
			}
			if(flag ==0)
				System.out.println(arr1[i]);
		}
		for(int i=0; i<arr2.length; i++){
			int flag =0;
			for(int j=0; j<arr1.length; j++){
				if(arr2[i]==arr1[j]){
					flag =1;
				}
			}
			if(flag ==0)
				System.out.println(arr2[i]);
		}
	}
}


