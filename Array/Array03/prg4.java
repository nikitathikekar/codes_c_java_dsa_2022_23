/* WAP to find perfect number from an Array and return its index*/

import java.io.*;
class Perfect{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i=0; i<arr.length; i++){
			int sum=0;
			for(int j=1; j<arr[i]; j++){

				if(arr[i] %j ==0){
					sum = sum+j;
				}
			}
			if(sum==arr[i]){
				System.out.println("Perfect No. "+arr[i]+ " Found at Index "+i);
			}
		}
	}
}


