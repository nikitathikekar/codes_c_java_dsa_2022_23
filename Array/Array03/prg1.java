/* WAP to find composite number in an Array & return its index 
 Take size from the user
 I/p-1  2  3  5  6  7
 o/p - Composite 6 found at index 4*/

import java.io.*;
class Composite{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Composite cmp = new Composite();
		System.out.println("Enter Size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter Elements of An Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = cmp.fun(arr);
		if(ret == -1){
			System.out.println("Composite No. not found in given array");
		}else{
			System.out.println("Composite no."+arr[ret]+" Found at Index "+ret);
		}
	}
	int fun(int arr[]){
		for(int i=0; i<arr.length; i++){
			int count =0;
			for(int j=1; j<=arr[i]; j++){
				if(arr[i] % j==0){
					count++;
				}
			}
			if(count>2){
				return i;
			}
		}
		return -1;
	}
}



