/*WAP to print count of digits in element of array
 I/p - 02  255  2  1554
 O/p - 2 3 1 4*/

import java.io.*;
class CountOfDigits{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter Elements Of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		for(int i=0; i<arr.length; i++){
			int count=0;
			while(arr[i]>0){
				count++;
				arr[i]=arr[i]/10;
			}
			System.out.println(count);
		}
	}
}




