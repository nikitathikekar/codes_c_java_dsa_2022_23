/* WAP to find a Armstrong  number from an Array and return its index*/

import java.io.*;
class Armstrong{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = fun(arr);
		if(ret == -1)
			System.out.println("Armstrong No. is not found in given array");
		else
			System.out.println("Armstrong No. "+arr[ret]+ " Found at Index "+ret);
	}
	static int fun(int arr[]){

		for(int i=0; i<arr.length; i++){
			int temp1=arr[i],temp2=arr[i],sum=0,count=0;
			while(temp1!=0){
				count++;
				temp1=temp1/10;
			}
			while(temp2 !=0){
				int cube=1;
				int rem = temp2%10;
				for(int j=1; j<=count; j++){
					cube = cube*rem;
				}
				sum = sum+cube;
				temp2=temp2/10;
			}
			if(arr[i]==sum){
				return i;
			}
		}
		return -1;
	}
}


