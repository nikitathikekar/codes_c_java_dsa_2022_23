/* WAP to print reverse of each element in an Array
 I/p - 10 25 252 36 564
 O/p - 01 52 252 63 456
 */

import java.util.*;
class Reverse{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		Reverse obj = new Reverse();
		System.out.println("Enter size of an Array");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println("Array Elements are: ");
		for(int i=0; i<arr.length; i++){
		       System.out.println(arr[i]);
		}

		int[] ret =obj.rev(arr);
		System.out.println("Reverse Element in array: ");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}

	}
	int[] rev(int arr[]){
		for(int i=0; i<arr.length; i++){
			int x=0;
			while(arr[i]!=0){
				int rem = arr[i]%10;
				x = x*10+rem;
				arr[i] = arr[i]/10;
			}
			arr[i] = x;
		}
		return arr;	
	}
}
