/* WAP to find prime no. from an array and return its index. Take size and element from the user 
I/p-10 25 36 566 34 53 20 100
O/p - Prime no 53 found at index 5
*/

import java.util.*;
class Prime{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of an array");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		for(int i=0; i<arr.length; i++){

			int count =0;
			for(int j=1; j<=arr[i]; j++){
				if(arr[i]%j==0){
					count++;
				}
			}if(count==2){
				System.out.println("Prime No. "+arr[i]+" Found at index "+i);
			}
		}
	}
}

