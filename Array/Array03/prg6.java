/* WAP to find palindrome number from an Array and return its index*/

import java.io.*;
class Palindrome{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = palin(arr);
		if(ret == -1)
			System.out.println("Palindrome is not found in given array");
		else
			System.out.println("Palindrome No. "+arr[ret]+ " Found at Index "+ret);
	}
	static int palin(int arr[]){

		for(int i=0; i<arr.length; i++){
			int temp=arr[i],rev=0;
			while(temp !=0 ){
				int rem = temp%10;
				rev = rev*10+rem;
				temp = temp/10;
			}

			if(arr[i]==rev){
				return i;
			}
		}
		return -1;
	}
}


