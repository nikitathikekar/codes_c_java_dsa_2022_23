/* WAP to find a strong  number from an Array and return its index*/

import java.io.*;
class Strong{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter size of an Array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		System.out.println("Enter elements of an Array");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ret = fun(arr);
		if(ret == -1)
			System.out.println("Strong No. is not found in given array");
		else
			System.out.println("Strong No. "+arr[ret]+ " Found at Index "+ret);
	}
	static int fun(int arr[]){

		for(int i=0; i<arr.length; i++){
			int temp=arr[i],sum=0;
			while(temp !=0){
				int fact=1;
				int rem = temp%10;
				for(int j=1; j<=rem; j++){
					fact = fact*j;
				}
				sum = sum+fact;
				temp=temp/10;
			}
			if(arr[i]==sum){
				return i;
			}
		}
		return -1;
	}
}


