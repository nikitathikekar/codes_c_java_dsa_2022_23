
class Course{

	static int fees = 10000;

	int noCodeExecuted = 300;
	String nameOfStudent = "Neha";

	void courseInfo(){
		System.out.println("name Of the Student= "+nameOfStudent);
		System.out.println("Count of code Executed by paricular Student= "+noCodeExecuted);
		System.out.println("Fees= "+fees);
	}
}
class Student{
	public static void main(String[] args){
		Course std1 = new Course();
		Course std2 = new Course();
	
		std1.courseInfo();
		std2.courseInfo();

		System.out.println(".............");

		std2.fees=20000;
		std2.nameOfStudent = "Ravi";
		std2.noCodeExecuted = 200;

		std1.courseInfo();
		std2.courseInfo();
	}
}

